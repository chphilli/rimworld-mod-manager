package com.gerusz.rwmm.utils;

import com.gerusz.rwmm.data.SavegameList;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class SavegameLoader extends DefaultHandler {

	private final String fileName;
	private String currentString;
	private String gameVersion;
	private List<String> modFolders = new ArrayList<>();
	private List<String> modNames = new ArrayList<>();
	private boolean readingModFolders = false;
	private boolean readingModNames = false;
	@Getter
	private SavegameList.Savegame savegame;

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (localName) {
			case "meta": {
				savegame = new SavegameList.Savegame(fileName, gameVersion, modFolders, modNames);
				throw new StopParsingException();
			}
			case "gameVersion": {
				gameVersion = currentString.split("\\s+")[0];
				break;
			}
			case "li": {
				if (readingModFolders) {
					modFolders.add(currentString);
					break;
				}
				if (readingModNames) {
					modNames.add(currentString);
					break;
				}
				super.endElement(uri, localName, qName);
				break;
			}
			case "modIds": {
				readingModFolders = false;
				break;
			}
			case "modNames": {
				readingModNames = false;
				break;
			}
		}
		super.endElement(uri, localName, qName);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentString = "";
		switch (localName) {
			case "modIds": {
				readingModFolders = true;
				break;
			}
			case "modNames": {
				readingModNames = true;
				break;
			}
		}
		super.startElement(uri, localName, qName, attributes);
	}

	public static class StopParsingException extends SAXException {
		// Don't care. Just stop.
	}
}
