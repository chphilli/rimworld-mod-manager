package com.gerusz.rwmm.utils;

import com.gerusz.rwmm.RwmmApplication;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.logging.Level;

@Log
public class ModVersionMatcher {

	public static boolean matches(String versionsString, String gameVersion) {
		String[] versions = versionsString.trim().split(", ");
		String[] filterVersionComponents = gameVersion.split("\\.");
		return Arrays.stream(versions).anyMatch(version -> {
			try {
				String[] versionComponents = version.split("\\.");
				return Integer.valueOf(filterVersionComponents[0]).equals(Integer.valueOf(versionComponents[0])) &&
				       Integer.valueOf(filterVersionComponents[1]).equals(Integer.valueOf(versionComponents[1]));
			} catch (Exception e) {
				log.log(Level.WARNING, "Can't match version", e);
				return false;
			}
		});
	}

	public static boolean matches(String versionsString) {
		return matches(versionsString, RwmmApplication.gameVersion);
	}
}
