package com.gerusz.rwmm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ModManagerConfig {
	private String internalModsFolder;
	private String modListPath;
	private String savegameFolder;
	private String steamModsFolder;
}
