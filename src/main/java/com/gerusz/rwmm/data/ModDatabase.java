package com.gerusz.rwmm.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gerusz.rwmm.ModManagerConfig;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Log
public class ModDatabase {

	public final static Map<String, RimworldMod> modsByFolder = new HashMap<>();
	private static final XmlMapper mapper = new XmlMapper();
	private static List<String> missingMods = new ArrayList<>();

	public static void loadMods(ModManagerConfig config, SavegameList.Savegame savegame) {
		ActiveModList savegameModList = new ActiveModList(savegame.getModFolders(), savegame.getGameVersion());
		loadMods(config, savegameModList);
		for(int i=0; i<missingMods.size(); i++) {
			String missingModFolder = missingMods.get(i);
			int missingModIndex = savegame.getModFolders().indexOf(missingModFolder);
			if(missingModIndex != -1 && missingModIndex < savegame.getModNames().size()) {
				missingMods.remove(i);
				missingMods.add(i, savegame.getModNames().get(missingModIndex));
			}
		}
	}

	public static void loadMods(ModManagerConfig config, ActiveModList activeModList) {
		File currentModList = new File(config.getInternalModsFolder());
		if (currentModList.exists() && currentModList.isDirectory()) {
			File[] modFoldersList = currentModList.listFiles(File::isDirectory);
			if (modFoldersList != null) {
				List<RimworldMod> mods = loadMods(modFoldersList, RimworldMod.ModType.INTERNAL, activeModList, missingMods);
				mods.forEach(mod -> modsByFolder.put(mod.getFolderName(), mod));
			}
		}
		else if (!currentModList.exists()) {
			log.severe("The internal mod list is invalid (folder does not exist)! This will mess up the mod list as the Core won't be found, so bye-bye!");
			System.exit(-4);
		}
		else {
			log.severe("The internal mod list is invalid (it exists but not a folder)! This will mess up the mod list as the Core won't be found, so bye-bye!");
			System.exit(-4);
		}

		currentModList = new File(config.getSteamModsFolder());
		if (currentModList.exists() && currentModList.isDirectory()) {
			File[] modFoldersList = currentModList.listFiles(File::isDirectory);
			if (modFoldersList != null) {
				List<RimworldMod> mods = loadMods(modFoldersList, RimworldMod.ModType.STEAM, activeModList, missingMods);
				mods.forEach(mod -> modsByFolder.put(mod.getFolderName(), mod));
			}
		}
		else if (!currentModList.exists()) {
			log.severe("The Steam mod list is invalid (folder does not exist)! This will mess up the mod list as the Core won't be found, so bye-bye!");
			System.exit(-4);
		}
		else {
			log.severe("The Steam mod list is invalid (it exists but not a folder)! This will mess up the mod list as the Core won't be found, so bye-bye!");
			System.exit(-4);
		}
	}

	public static Map<String, RimworldMod> queryMap(ModDbQuery query) {
		return modsByFolder.entrySet().stream().filter(query::matches).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public static List<RimworldMod> queryValues(ModDbQuery query) {
		return modsByFolder.values().stream().filter(query::matches).collect(Collectors.toList());
	}

	private static RimworldMod loadMod(String modFolderPath, RimworldMod.ModType modType, ActiveModList activeModList, List<String> missingMods)
			throws IOException {
		File modFolder = new File(modFolderPath);
		String modFolderName = modFolderPath.substring(modFolderPath.lastIndexOf(File.separator) + 1);
		if (!modFolder.exists()) {
			log.info("Missing mod (unsubscribed?) : " + modFolderPath);
			missingMods.add(modFolderName);
			return null;
		}
		String aboutFilePath = modFolderPath + File.separator + "About" + File.separator + "About.xml";
		File aboutFile = new File(aboutFilePath);
		if (!aboutFile.exists()) {
			log.info("About file doesn't exist (unsubscibed?): " + aboutFilePath);
			missingMods.add(modFolderName);
			return null;
		}
		AboutFileContents aboutFileContents = mapper.readValue(aboutFile, AboutFileContents.class);
		RimworldMod mod = new RimworldMod();
		mod.setFolderName(modFolderName);
		mod.setName(aboutFileContents.name);
		mod.setActive(activeModList.activeMods.contains(modFolderName));
		if (modType == RimworldMod.ModType.INTERNAL && modFolderName.equals("Core")) {
			mod.setModType(RimworldMod.ModType.CORE);
			mod.setVersions(new String[]{activeModList.version});
		}
		else {
			mod.setModType(modType);
			mod.setVersions(CollectionUtils.isEmpty(aboutFileContents.supportedVersions) ?
			                new String[]{aboutFileContents.targetVersion} :
			                aboutFileContents.supportedVersions.toArray(new String[]{}));
		}
		mod.setAboutFileContents(aboutFileContents);
		return mod;
	}

	private static List<RimworldMod> loadMods(File[] modFolderList,
	                                          RimworldMod.ModType modType,
	                                          ActiveModList activeModList,
	                                          List<String> missingMods) {
		List<RimworldMod> output = new ArrayList<>();
		for (File modFolder : modFolderList) {
			try {
				RimworldMod mod = loadMod(modFolder.getAbsolutePath(), modType, activeModList, missingMods);
				if (mod != null) {
					output.add(mod);
				}
			} catch (Exception e) {
				log.warning("Mod " + modFolder.getAbsolutePath() + " could not be loaded!");
			}
		}
		return output;
	}

	@NoArgsConstructor
	public static class ModDbQuery {
		private Boolean activeFilter = null;
		private String gameVersionFilter = null;
		private RimworldMod.ModType modTypeFilter = null;

		public ModDbQuery active(boolean activeFilter) {
			this.activeFilter = activeFilter;
			return this;
		}

		public ModDbQuery gameVersion(String gameVersionFilter) {
			this.gameVersionFilter = gameVersionFilter;
			return this;
		}

		public boolean matches(Map.Entry<String, RimworldMod> mapEntry) {
			return matches(mapEntry.getValue());
		}

		public boolean matches(RimworldMod mod) {
			boolean matches = true;
			if (activeFilter != null) {
				matches &= mod.isActive() == activeFilter;
			}
			if (modTypeFilter != null) {
				matches &= mod.getModType() == modTypeFilter;
			}
			if (gameVersionFilter != null) {
				String[] filterVersionComponents = gameVersionFilter.split("\\.");
				matches &= Arrays.stream(mod.getVersions()).anyMatch(version -> {
					try {
						String[] versionComponents = version.split("\\.");
						return filterVersionComponents[0].equals(versionComponents[0]) && filterVersionComponents[1].equals(versionComponents[1]);
					} catch (Exception e) {
						log.warning("Mod " + mod.getName() + " (folder: " + mod.getFolderName() + ") apparently doesn't have a version...");
						return false;
					}
				});
			}
			return matches;
		}

		public ModDbQuery modType(RimworldMod.ModType modTypeFilter) {
			this.modTypeFilter = modTypeFilter;
			return this;
		}
	}
}
