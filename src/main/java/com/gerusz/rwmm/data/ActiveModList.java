package com.gerusz.rwmm.data;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("ModsConfigData")
@JsonPropertyOrder({"version", "activeMods"})
public class ActiveModList {
	@JacksonXmlElementWrapper(localName = "activeMods")
	@JacksonXmlProperty(localName = "li")
	public List<String> activeMods;
	public String version;
}
